export function isvalidUsername(str) {
  return /^[a-zA-Z]*?/.test(str)
  /*
  const valid_map = ['admin', 'editor']
  return valid_map.indexOf(str.trim()) >= 0
 */
}

/* 合法uri*/
export function validateURL(textval) {
  // const urlregex = /^(https?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/
  var urlregex = '^((https|http|ftp|rtsp|mms)?://)'
        + '?(([0-9a-z_!~*\'().&=+$%-]+: )?[0-9a-z_!~*\'().&=+$%-]+@)?' //ftp的user@ 
        + '(([0-9]{1,3}.){3}[0-9]{1,3}' // IP形式的URL- 199.194.52.184 
        + '|' // 允许IP和DOMAIN（域名） 
        + '([0-9a-z_!~*\'()-]+.)*' // 域名- www. 
        + '([0-9a-z][0-9a-z-]{0,61})?[0-9a-z].' // 二级域名 
        + '[a-z]{2,6})' // first level domain- .com or .museum 
        + '(:[0-9]{1,4})?' // 端口- :80 
        + '((/?)|' // a slash isn't required if there is no file name 
        + '(/[0-9a-z_!~*\'().;?:@&=+$,%#-]+)+/?)$'
  var re = new RegExp(urlregex);
  return re.test(textval)
}

/* 小写字母*/
export function validateLowerCase(str) {
  const reg = /^[a-z]+$/
  return reg.test(str)
}

/* 大写字母*/
export function validateUpperCase(str) {
  const reg = /^[A-Z]+$/
  return reg.test(str)
}

/* 大小写字母*/
export function validateAlphabets(str) {
  const reg = /^[A-Za-z]+$/
  return reg.test(str)
}

/* 正整数 */
export function validateInt(str) {
  const reg = /^[1-9][0-9]*$/g
  return reg.test(str);
}

/* 正数 */
export function validatePositiveNum(str) {
  if (str > 0 && !isNaN(str)) {
    return true;
  } else {
    return false;
  }
}

/**
 * 数字、字母或汉字
 * @method validateGeneral
 * @param str {String}      # 待验证字符串
 * @param len {String}      # 验证长度
 */
export function validateGeneral(str, len) {
  const reg = /^[A-Za-z\u4E00-\u9FA5\d]+$/;
  return (str.length <= len) && reg.test(str);
}
