/*
 * @Descripttion: 
 * @version: 1.0.0
 * @Author: guoxiaomin
 * @Email: 1093556028@qq.com
 * @Date: 2019-08-21 14:59:42
 * @LastEditors: guoxiaomin
 * @LastEditTime: 2019-09-12 18:01:34
 */
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import Service from './common/service'
Vue.config.productionTip = false
Vue.prototype.$service = Service
Vue.use(ElementUI)
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
