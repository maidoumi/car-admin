/*
 * @Descripttion: 
 * @version: 1.0.0
 * @Author: guoxiaomin
 * @Email: 1093556028@qq.com
 * @Date: 2019-08-21 14:59:42
 * @LastEditors: guoxiaomin
 * @LastEditTime: 2019-09-12 15:28:32
 */
import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home'
import Login from './views/login'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    }
  ]
})
