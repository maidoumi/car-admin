####使用
``` bash
# 引入
    import List from "../layout/components/List/index";
# 注册
    components: {
        List,
    }
# 使用
    <List
        :title="title"
        :tableData="tableData"
        :operate="operate"
        :pageSize="rn"
        :total="total"
        :pageNum="pn"
        @changeOp="changeOp"
        @changePn="changePn"
        :columnStyle="columnStyle"
        :tag="tag"
        ope_length="50"
    ></List>
```
####参数说明
``` bash
    # title，数组
    title: [
      {
        type: "name",  //表头字段key值
        value: "名称", // 表头展示字段
        width: "83", // 最低宽度
        vtype: "tag", // 可选html，tag，默认text,
                            若为html，可直接跳转，须后端返回数据格式为"<a href='http://kcadmin.rdtest.xuanke.com'>5</a>"
                            若为tag，显示标签，需在组件内传入tag，默认
                            tag: {
                                "未生效": "primary",
                                "已生效": "success",
                            }
    },
    ]
    # tableData，数组
    列表数据
    # operate，数组，右侧操作栏
    operate: [
        {
          key: "operation", // 操作标识
          value: "编辑", // 按钮文案
          evalue: "发放", // change为true时按钮的可变文案
          change: true // 设置为true后，根据status状态显示不同文案，对应不同操作
          can: true  // 设置为true后，根据status状态判断按钮是否可操作，
        },
        {
          key: "record",
          value: "使用记录"
        },
        {
          key: "edit",
          value: "发布",
          evalue: "下线",
          change: true 
        }
      ]
    # pageSize，Number类型，每页数据条数，默认0
        rn：20；
    # total，Number类型，数据总数，默认0
        total： 0；
    # pageNum，Number类型，页码，默认0
        pn：0；
    # changeOp，Function，操作栏点击触发，
    # 返回（index，data）
    # index为点击的操作列表的下标，data为点击列表的行数据
    # 以上述传入operate为例，操作如下
    /**
     * 列表操作
     * @param index 操作类型 0编辑/发放 2使用记录 3下线/发布
     * @param data 操作数据
     */
    changeOp(index, data) {
      const id = data.row.id;
      if (index === 0) {
        if (data.row.status) {
          //跳转发放
          this.$router.push({
            path: "/coupon/grant",
            query: { id }
          });
        } else {
          // 跳转编辑
          this.$router.push({
            path: "/coupon/edit",
            query: { id }
          });
        }
      } else if (index === 1) {
        // 使用记录
        this.$router.push({
          path: "/coupon/record",
          query: { id }
        });
      } else {
        // 上下线
        if (!data.row.status) {
          this.goPublish({ id });
        } else {
          this.goOffline({ id });
        }
      }
    },
    # changePn，Function，分页组件页码点击触发
    # 返回num，为点击的页面
    /**
     * 分页点击
     * @param num 分页点击页码
     */
    changePn(num) {
      this.pn = num;
      this.toGetList();
    },
    # columnStyle，Object，列表数据某一列自定义样式
    columnStyle: {
          column: 1, // 列下标，从0开始
          style: { // 自定义样式
            color: "#409eff",
          },             
      },
     # tag， Object，当列表数据有标签时需要传入的标签样式
     tag: {
        "未支付": "warning",
        "超时未支付": "info",
        "拼团中": "",
        "拼团成功": "success",
        "退款中": "info",
        "已退款": "",
        "退款失败": "danger",
      },
    # ope_length，操作栏宽度
    ope_length="150"
```