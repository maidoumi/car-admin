####使用
``` bash
# 引入
    import Search from "../../layout/components/Search/index";
# 注册
    components: {
        Search
    }
# 使用
    <Search 
        :title="s_title" 
        :searchForm="searchForm" 
        @submit="toSearch" 
        :add="add"
        @add="add"
        :needexport="needexport"
        @toExport="toExport"
        :needReset="needReset"
        @reset="reset"
    />
```
####参数说明
``` bash
    # title，数组
    title: [
        pay_id: {
          key: "pay_id",  // 搜索key值，和searchForm中key对应
          type: "text", // 搜索项类型，可选text，textarea，select，timepicker，                         datatimepicker
          value: "支付ID", // 搜索项title
          placeholder: "请输入支付ID" // 搜索项默认文案
        },
        status: {
          key: "status",
          type: "select",
          value: "订单状态",
          placeholder: "请选择订单状态",
          data: [                          // 搜索类型为select时传入的下拉选项数组
            {id: 0, name: "未支付"},
            {id: 7, name: "超时未支付"},
            {id: 1, name: "拼团中"},
            {id: 2, name: "拼团成功"},
            {id: 4, name: "退款中 "},
            {id: 6, name: "已退款"},
            {id: 3, name: "退款失败"}
          ]
        },
        time: {
          key: "time",
          type: "datatimepicker",
          value: "支付时间",
        }
    ]    
    # searchForm，对象，搜索数据，key值与title中key对应
    searchForm: {
        pay_id: "",
        status: "",
        time: [],
      },
    # submit，Function，点击搜索触发事件
    #         返回searchForm
    toSearch(data) {
      this.searchForm = data
      this.toGetList();
    },
    # add，Boolean类型，是否需要右侧的新增按钮，默认false
        add：false；
    # add，Function类型，有新增按钮时的点击事件
        add: () => {
            // 操作
        }
    # needexport，Boolean类型，是否需要导出按钮，默认false
        needexport： false；
    # toExport，Function类型，有导出按钮时的点击事件，返回searchForm搜索项
       toExport: (data) => {
            // 操作
        }
    # needReset，Boolean类型，是否需要重置按钮，默认true
        needReset： false；
    # reset，Function类型，有重置按钮时的点击事件，返回搜索项
       reset: (data) => {
            // 操作
        }
```