/*
 * @Descripttion: 配置文件
 * @version: 1.0.0
 * @Author: guoxiaomin
 * @Email: 1093556028@qq.com
 * @Date: 2019-08-20 10:38:02
 * @LastEditors: guoxiaomin
 * @LastEditTime: 2019-08-21 15:22:22
 */
module.exports = {
  baseUrl: process.env.NODE_ENV === 'production' ? './' : '/',
  // 输出文件目录
  outputDir: 'admindist',
  // webpack-dev-server 相关配置
  devServer: {
    port: 8900,
    proxy: {
      'user': {
        target: 'http://123.206.16.150',
        changeOrigin: true,
        secure: false
      },
      'api/kyzz/dream': {
        target: 'http://47.94.139.210:8901/dream',
        changeOrigin: true,
        pathRewrite: {
          '^/api/kyzz/dream': ''
        },
        secure: false
      }
    }
  },
  lintOnSave: false
  // chainWebpack: config => {
  //   config.rule('js').include.add(/node_modules\/(dom7|swiper)\/.*/)
  // }
}
